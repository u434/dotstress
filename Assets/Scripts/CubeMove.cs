using UnityEngine;

public class CubeMove : MonoBehaviour {
    void Update()
    {
        Vector3 pos = transform.position;
        pos += transform.right * GameManager.Instance.GetMoveSpeed() * Time.deltaTime;

        transform.position = pos;
    }
}
