using UnityEngine;

public class CameraMovement : MonoBehaviour {
    float mouseSensitivity = 3;
    float moveSpeed = 20;

    private void Start() {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update() {
        RotateCamera();
        MoveCamera();
    }

    void RotateCamera() {
        //TESTING
        return;

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        float rotAmountX = mouseX * mouseSensitivity;
        float rotAmountY = mouseY * mouseSensitivity;

        Vector3 rotCamera = transform.rotation.eulerAngles;

        rotCamera.y += rotAmountX;
        rotCamera.x -= rotAmountY;
        rotCamera.z = 0;

        if (rotCamera.x > 85 && rotCamera.x < 180) {
            rotCamera.x = 85;
        }
        if (rotCamera.x < 275 && rotCamera.x > 180) {
            rotCamera.x = 275;
        }

        transform.rotation = Quaternion.Euler(rotCamera);
    }

    void MoveCamera() {
        //SpeedBoost
        float speedModifier = 1;
        if (Input.GetKey(KeyCode.LeftShift)) {
            speedModifier = 5;
        }

        //Forward/Backwards
        if (Input.GetKey(KeyCode.W)) {
            transform.position += transform.forward * moveSpeed * Time.deltaTime * speedModifier;
        }
        else if (Input.GetKey(KeyCode.S)) {
            transform.position -= transform.forward * moveSpeed * Time.deltaTime * speedModifier;
        }

        //Left/Right
        if (Input.GetKey(KeyCode.D)) {
            transform.position += transform.right * moveSpeed * Time.deltaTime * speedModifier;
        }
        else if (Input.GetKey(KeyCode.A)) {
            transform.position -= transform.right * moveSpeed * Time.deltaTime * speedModifier;
        }
    }
}
