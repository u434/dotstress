using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour {
    //Text fields
    [SerializeField] private Text mode;
    [SerializeField] private Text jobsBurst;
    [SerializeField] private Text units;
    [SerializeField] private Text FPS;
    [SerializeField] private Text FPSLog;

    //FPS
    //private float deltaTime = 0.0f;
    private float frameTimes = 0;
    private int frame = 0;
    private int frameSample = 50;

    void Update() {
        //Average the deltaTime for roughly ten frames (seconds per frame)
        //deltaTime += (Time.unscaledDeltaTime - deltaTime) * 1f;
        //float fps = 1.0f / deltaTime;
        //fps = (int)(fps * 100) / 100f;

        if(frame < frameSample)
        {
            frameTimes += Time.deltaTime;
        }
        else
        {
            float fps = frameTimes / frameSample;
            fps = 1f / fps;
            fps *= 10f;
            fps = (int)fps;
            fps /= 10f;
            FPS.text = "FPS: " + fps;
            Debug.Log("Frame average: " + fps);
            FPSLog.text = fps + "\n" + FPSLog.text;

            frameTimes = Time.deltaTime;
            frame = 0;
        }

        frame++;

        // round up
        //string text = Mathf.Ceil(fps).ToString();
        //FPS.text = "FPS: " + text;
        //FPS.text = "FPS: " + fps;
    }

    public void SetTotal(int total) {
        units.text = "Units: " + total;
    }

    public void SetMode(bool jobs, bool burst) {
        //Mode
        switch (GameManager.Instance.mode) {
            case DemoMode.Classic:
                mode.text = "Mode: Classic";
                break;
            case DemoMode.ECSConversion:
                mode.text = "Mode: ECS Conversion";
                break;
            case DemoMode.ECSPure:
                mode.text = "Mode: ECS Pure";
                break;
        }

        //Jobs-Burst
        if (GameManager.Instance.mode != DemoMode.Classic) {
            jobsBurst.gameObject.SetActive(true);
            string jobsString;
            string burstString;

            if (jobs) {
                jobsString = "Jobs: Yes";
            }
            else {
                jobsString = "Jobs: No";
            }

            if (burst) {
                burstString = "Burst: Yes";
            }
            else {
                burstString = "Burst: No";
            }

            jobsBurst.text = jobsString + "\n" + burstString;
        }
        else {
            jobsBurst.gameObject.SetActive(false);
        }
    }
}
