using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;

public class Spawner : MonoBehaviour {
    //Stats
    private int totalSpawned = 0;   //Total number of cubes spawned so far
    private int spawnCounter = 0;   //Which planet needs to be spawned
    private float mercuryRadius = 3f;   //Base radius of smallest planet
    private float[] radiusScale = { 1f, 1.39f, 2.48f, 2.61f, 10.09f, 10.4f, 23.87f, 28.66f, 285.42f};   //Relative size of planets to base radius
    private float[] realRadius = { 2439.7f, 3389.5f, 6051.8f, 6371f, 24622f, 25362f, 58232f, 69911f, 969340f };   //Real radius of planets in km
    private string[] planetName = { "Mercury", "Mars", "Venus", "Earth", "Neptune", "Uranus", "Saturn", "Jupiter", "Sun" };   //Name of planets
    [SerializeField] private Material[] planetMaterial;
    // Mercury, Mars, Venus, Earth, Neptune, Uranus, Saturn, Jupiter, Sun

    private float offset = 0;   //Offset, how much next planet needs to be offset to not overlap with old (x plane)

    //Prefabs
    [SerializeField] private GameObject ECSPrefab;
    [SerializeField] private GameObject classicPrefab;
    [SerializeField] private Mesh cubeMesh;
    [SerializeField] private Material cubeMaterial;
    [SerializeField] private GameObject textDescription;

    //Variables
    private World defaultWorld;
    private EntityManager entityManager;
    private EntityArchetype archetype;
    private Entity unitEntityPrefab;

    #region Setup
    void Start() {
        if (GameManager.Instance.mode == DemoMode.ECSConversion) {
            SetupECSConversion();
        }

        if (GameManager.Instance.mode == DemoMode.ECSPure) {
            SetupECSPure();
        }

        SpawnUnit(-2);
    }

    private void SetupECSPure() {
        if (GameManager.Instance.mode == DemoMode.ECSPure) {
            defaultWorld = World.DefaultGameObjectInjectionWorld;
            entityManager = defaultWorld.EntityManager;
            archetype = entityManager.CreateArchetype
            (
                typeof(Translation),
                typeof(Rotation),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(CubeData),
                typeof(Scale),
                typeof(RenderBounds)
            );
        }
    }

    private void SetupECSConversion() {
        if (GameManager.Instance.mode == DemoMode.ECSConversion) {
            defaultWorld = World.DefaultGameObjectInjectionWorld;
            entityManager = defaultWorld.EntityManager;
            GameObjectConversionSettings settings = GameObjectConversionSettings.FromWorld(defaultWorld, null);
            unitEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(ECSPrefab, settings);
        }
    }
    #endregion

    #region Spawning
    public void SpawnUnit(int index) {
        //Index -1 means spawn next planet, otherwise it aims specific planet
        //Index -2 means dont spawn anything
        //If no more planets to spawn
        if((spawnCounter > 8 && index == -1) || index == -2) {
            return;
        }

        int debugIndex = index;
        if(index == -1) {
            debugIndex = spawnCounter;
        }
        //Just debug
        if(debugIndex == 0) {
            Debug.Log("Spawning Mercury! Radius 2,439.7km. Scale: " + radiusScale[spawnCounter]);
        }
        else if (debugIndex == 1) {
            Debug.Log("Spawning Mars! Radius 3,389.5km. Scale: " + radiusScale[spawnCounter]);
        }
        else if (debugIndex == 2) {
            Debug.Log("Spawning Venus! Radius 6,051.8km. Scale: " + radiusScale[spawnCounter]);
        }
        else if (debugIndex == 3) {
            Debug.Log("Spawning Earth! Radius 6,371km. Scale: " + radiusScale[spawnCounter]);
        }
        else if (debugIndex == 4) {
            Debug.Log("Spawning Neptune! Radius 24,622km. Scale: " + radiusScale[spawnCounter]);
        }
        else if (debugIndex == 5) {
            Debug.Log("Spawning Uranus! Radius 25,362km. Scale: " + radiusScale[spawnCounter]);
        }
        else if (debugIndex == 6) {
            Debug.Log("Spawning Saturn! Radius 58,232km. Scale: " + radiusScale[spawnCounter]);
        }
        else if (debugIndex == 7) {
            Debug.Log("Spawning Jupiter! Radius 69,911km. Scale: " + radiusScale[spawnCounter]);
        }
        else if (debugIndex == 8) {
            Debug.Log("Spawning Sun! Radius 969,340km. Scale: " + radiusScale[spawnCounter]);
        }

        switch (GameManager.Instance.mode) {
            case DemoMode.Classic:
                SpawnClassic(index);
                break;
            case DemoMode.ECSConversion:
                SpawnECSConversion(index);
                break;
            case DemoMode.ECSPure:
                SpawnECSPure(index);
                break;
        }
    }

    private void SpawnClassic(int index) {
        //Prepare
        Vector3 center = Vector3.zero;

        int planetIndex = spawnCounter;
        if (index != -1) {
            planetIndex = index;
        }
        
        float radius = mercuryRadius * radiusScale[planetIndex];
        int radiusInt = (int)radius + 1;
        int spawnedNow = 0;

        //To not overlap
        offset += radius * 1.2f;
        float lowestY = 0;   //Stores lowest y position of cube, so text goes underneath

        for (int i = -radiusInt; i <= radiusInt; i++) {
            for (int j = -radiusInt; j <= radiusInt; j++) {
                for (int k = -radiusInt; k <= radiusInt; k++) {
                    Vector3 position = new Vector3(i, j, k);
                    if (Vector3.Distance(center, position) < radius) {
                        spawnedNow++;
                        GameObject instance = Instantiate(classicPrefab);
                        instance.GetComponent<MeshRenderer>().material = planetMaterial[planetIndex];
                        Vector3 offsetPosition = new Vector3(position.x + offset, position.y, position.z);
                        instance.transform.position = offsetPosition;

                        if(position.y < lowestY) {
                            lowestY = position.y;
                        }
                    }
                }
            }
        }

        //Planet info
        GameObject info = Instantiate(textDescription);
        Vector3 textPosition = new Vector3(0f + offset, lowestY - 5f, 0f);
        info.transform.position = textPosition;
        info.GetComponent<TextMesh>().text = planetName[planetIndex] + "\nRadius: " + realRadius[planetIndex] + "km\nSpawned cubes: " + spawnedNow;

        if(index == -1) {
            spawnCounter++;
        }
        offset += radius * 1.2f;
        totalSpawned += spawnedNow;
        GameManager.Instance.ui.SetTotal(totalSpawned);
    }

    private void SpawnECSPure(int index) {
        //Prepare
        Vector3 center = Vector3.zero;

        int planetIndex = spawnCounter;
        if (index != -1) {
            planetIndex = index;
        }

        float radius = mercuryRadius * radiusScale[planetIndex];
        int radiusInt = (int)radius + 1;
        int spawnedNow = 0;

        //To not overlap
        offset += radius * 1.2f;
        float lowestY = 0;   //Stores lowest y position of cube, so text goes underneath

        for (int i = -radiusInt; i <= radiusInt; i++) {
            for (int j = -radiusInt; j <= radiusInt; j++) {
                for (int k = -radiusInt; k <= radiusInt; k++) {
                    Vector3 position = new Vector3(i, j, k);
                    if (Vector3.Distance(center, position) < radius) {
                        spawnedNow++;
                        float3 offsetPosition = new float3(position.x + offset, position.y, position.z);

                        Entity myEntity = entityManager.CreateEntity(archetype);

                        entityManager.AddComponentData(myEntity, new Translation { Value = offsetPosition });
                        entityManager.AddComponentData(myEntity, new Scale { Value = 1 });

                        entityManager.AddSharedComponentData(myEntity, new RenderMesh {
                            mesh = cubeMesh,
                            material = planetMaterial[planetIndex]
                        });

                        entityManager.AddComponentData(myEntity, new CubeData { speed = GameManager.Instance.GetMoveSpeed() });

                        if (position.y < lowestY) {
                            lowestY = position.y;
                        }
                    }
                }
            }
        }

        //Planet info
        GameObject info = Instantiate(textDescription);
        Vector3 textPosition = new Vector3(0f + offset, lowestY - 5f, 0f);
        info.transform.position = textPosition;
        info.GetComponent<TextMesh>().text = planetName[planetIndex] + "\nRadius: " + realRadius[planetIndex] + "km\nSpawned cubes: " + spawnedNow;

        if (index == -1) {
            spawnCounter++;
        }
        offset += radius * 1.2f;
        totalSpawned += spawnedNow;
        GameManager.Instance.ui.SetTotal(totalSpawned);
    }

    private void SpawnECSConversion(int index) {
        //Prepare
        Vector3 center = Vector3.zero;

        int planetIndex = spawnCounter;
        if (index != -1) {
            planetIndex = index;
        }

        float radius = mercuryRadius * radiusScale[planetIndex];
        int radiusInt = (int)radius + 1;
        int spawnedNow = 0;

        //To not overlap
        offset += radius * 1.2f;
        float lowestY = 0;   //Stores lowest y position of cube, so text goes underneath

        for (int i = -radiusInt; i <= radiusInt; i++) {
            for (int j = -radiusInt; j <= radiusInt; j++) {
                for (int k = -radiusInt; k <= radiusInt; k++) {
                    Vector3 position = new Vector3(i, j, k);
                    if (Vector3.Distance(center, position) < radius) {
                        spawnedNow++;
                        float3 offsetPosition = new float3(position.x + offset, position.y, position.z);

                        Entity myEntity = entityManager.Instantiate(unitEntityPrefab);

                        entityManager.SetComponentData(myEntity, new Translation { Value = offsetPosition });
                        entityManager.SetSharedComponentData(myEntity, new RenderMesh {
                            mesh = cubeMesh,
                            material = planetMaterial[planetIndex]
                        });
                        entityManager.AddComponentData(myEntity, new CubeData { speed = GameManager.Instance.GetMoveSpeed() });

                        if (position.y < lowestY) {
                            lowestY = position.y;
                        }
                    }
                }
            }
        }

        //Planet info
        GameObject info = Instantiate(textDescription);
        Vector3 textPosition = new Vector3(0f + offset, lowestY - 5f, 0f);
        info.transform.position = textPosition;
        info.GetComponent<TextMesh>().text = planetName[planetIndex] + "\nRadius: " + realRadius[planetIndex] + "km\nSpawned cubes: " + spawnedNow;

        if (index == -1) {
            spawnCounter++;
        }
        offset += radius * 1.2f;
        totalSpawned += spawnedNow;
        GameManager.Instance.ui.SetTotal(totalSpawned);
    }
    #endregion
}
