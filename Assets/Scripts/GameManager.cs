using UnityEngine;
using Unity.Entities;

[System.Serializable]
public enum DemoMode {
    Classic,
    ECSPure,
    ECSConversion
}

public class GameManager : MonoBehaviour {
    //Singleton
    public static GameManager Instance;

    //References
    public UI ui;
    public Spawner spawner;

    //Stats
    float moveSpeed = 0.001f;

    //Classic vs ECS
    [Header("Settings")]
    [SerializeField] public DemoMode mode;
    [SerializeField] public bool useJobs;
    [SerializeField] public bool useBurst;

    #region Getters
    public float GetMoveSpeed() {
        return moveSpeed;
    }
    #endregion

    private void Awake() {
        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(gameObject);
        }

        ui = GetComponent<UI>();
        spawner = FindObjectOfType<Spawner>();
    }

    private void Start() {
        SetSystemMode();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            spawner.SpawnUnit(-1);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad1)) {
            spawner.SpawnUnit(0);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2)) {
            spawner.SpawnUnit(1);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad3)) {
            spawner.SpawnUnit(2);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad4)) {
            spawner.SpawnUnit(3);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad5)) {
            spawner.SpawnUnit(4);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad6)) {
            spawner.SpawnUnit(5);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad7)) {
            spawner.SpawnUnit(6);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad8)) {
            spawner.SpawnUnit(7);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad9)) {
            spawner.SpawnUnit(8);
        }
    }

    private void SetSystemMode() {
        ui.SetMode(useJobs, useBurst);

        World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MovementSystem>().Enabled = false;
        World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MovementSystemJobs>().Enabled = false;
        World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MovementSystemJobsBurst>().Enabled = false;

        if (mode != DemoMode.Classic) {
            if (!useJobs) {
                World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MovementSystem>().Enabled = true;
                Debug.Log("Enabling MovementSystem");
            }
            else if (useBurst) {
                World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MovementSystemJobsBurst>().Enabled = true;
                Debug.Log("Enabling MovementSystemJobsBurst");
            }
            else if (!useBurst) {
                World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<MovementSystemJobs>().Enabled = true;
                Debug.Log("Enabling MovementSystemJobs");
            }

        }
    }
}

