using UnityEngine;
using Unity.Entities;

public class Healer : MonoBehaviour {
    int hp = 100;
    float healTick = 2.5f;
    float timer = 0;

    // Update is called once per frame
    void Update() {
        if (timer < Time.time) {
            Heal();
            timer = Time.time + healTick;
        }
    }

    public void Heal() {
        hp = 100;
    }
}

public class HealerStation {
    Healer healer = new Healer();

    void Heal() {
        healer.Heal();
    }
}

[GenerateAuthoringComponent]
public struct HealerData : IComponentData {
    public int hp;
    public float healTick;
    public float timer;
}

public class HealingSystem : ComponentSystem {
    protected override void OnUpdate() {
        Entities.ForEach((ref HealerData healData) => {
            healData.timer += Time.DeltaTime;

            if(Time.ElapsedTime > healData.timer) {
                healData.hp = 100;
            }

            healData.timer = (float)Time.ElapsedTime + healData.healTick;
        });
    }
}