using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;

// By default, the Burst Compiler is enabled. Unity's Burst Compiler
// is built on top of the LLVM compiler, which is optimized for your
// hardware.  This allows you continue using C# code but closer to the
// performance of C++.  To the end-user, it's nearly invisible.  


public class MovementSystemJobsBurst : JobComponentSystem {
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = Time.DeltaTime;

        JobHandle jobHandle = Entities.
            ForEach((ref Translation trans, ref CubeData moveForward) =>
            {
                trans.Value += new float3(moveForward.speed * deltaTime, 0f, 0f);
            }).Schedule(inputDeps);

        return jobHandle;
    }
}