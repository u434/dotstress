using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;

// Here we switch to using the JobComponentSystem.  Though the logic is nearly
// identical to the MovementSystem, this takes advantages of multiple cores.
//
// To avoid race conditions, Unity breaks complex tasks into small jobs.
// The Job System places them into a queue and does the heavy lifting of
// multi-threaded code for you.

public class MovementSystemJobs : JobComponentSystem {
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = Time.DeltaTime;

        JobHandle jobHandle = Entities.
            WithoutBurst().
            ForEach((ref Translation trans, ref CubeData moveForward) =>
            {
                trans.Value += new float3(moveForward.speed * deltaTime, 0f, 0f);
            }).Schedule(inputDeps);

        return jobHandle;
    }
}