using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using UnityEngine;

// This demonstrates a simple System for moving the cubes forward in DOTS/ECS.
// The ComponentSystem only runs on the main thread, so this demonstrates ECS without Unity Jobs.
public class MovementSystem : ComponentSystem {
    protected override void OnUpdate()
    {
        Entities.ForEach((ref Translation trans, ref CubeData moveForward) =>
        {
            float deltaTime = Time.DeltaTime;

            trans.Value += new float3(moveForward.speed * deltaTime, 0f, 0f);
            //return;
            //Izracun
            float math = 1.21f;
            math = Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(math)))))));
            math = Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(math)))))));
            math = Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(Mathf.Sqrt(math)))))));
        });
    }
}
