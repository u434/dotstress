# DOTStress

Stress test for Unity DOTS feature. How DOTS and its components compare to the classic Unity GameObjects performance

## About

Project uses basic cubes to form balls that represent planets in solar system. Project displays which method is used, how many cubes are on the screen and current performance (FPS).

* Standard Unity GameObject performance on smaller number of objects
![Standard 1](gitData/classic_results_1.png?raw=true "Standard 1")

* Standard Unity GameObject performance on large number of objects
![Standard 2](gitData/classic_results_2.png?raw=true "Standard 2")

* Unity DOTS(ECS, Jobs, Burst) performance on large number of objects
![DOTS](gitData/ecs_results.png?raw=true "DOTS")